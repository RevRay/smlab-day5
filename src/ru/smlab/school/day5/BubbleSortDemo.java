package ru.smlab.school.day5;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class BubbleSortDemo {
    public static void main(String[] args) {

        //можно по очереди комментировать каждый из этих трех методов, чтобы отдельно посмотреть на ту или иную демонстрацию

        bubbleSortSimpleDemo();

        bubbleSortPerformanceDemo();

        binarySearchVsLinearSearchDemo();

    }

    public static void bubbleSortSimpleDemo() {
        //создали массив
        int[] array = {100, 23, 23, -4, 9, 10, 2, 7, 4, 3, 6, -12};
        //вывели в консоль значения до сортировки
        System.out.println("До сортировки: " + Arrays.toString(array));
        bubbleSort(array);
        //вывезли в консоль значения после сортировки
        System.out.println("После сортировки: " + Arrays.toString(array) + '\n');
    }

    public static void bubbleSortPerformanceDemo() {
        //сгенерировали массив из случайных чисел
        int length = 100000;
        int bound = 10000;
        int[] array = generateArray(length, bound);

        //#1 - замеряем время до выполнения сортировки пузырьком
        //аналогия - нажали на кнопку старта секундомера
        long beforeBubbleSort = System.currentTimeMillis();

        bubbleSort(array);

        //#2 - теперь замеряем время после выполнения сортировки пузырьком
        //аналогия - нажали на кнопку стоп секундомера
        long afterBubbleSort = System.currentTimeMillis();
        //#3 - разницу полученных значений выводим на экран
        System.out.println(String.format("Сортировка завершена. Длина переданного массива - '%d', диапазон чисел - 0..%d", length, bound));
        System.out.println(String.format("Время выполнения сортировки пузырьком составило: %d мс", afterBubbleSort - beforeBubbleSort) + '\n');
    }

    /**
     * Замеряем производительность линейного и бинарного поисков.
     *
     * Важно - если для обоих поисков будет выдаваться время '0 мс' - попробуйте увеличить
     * длину и диапазон массива генерируемых значений на 1-2 порядка.
     * После этого программа будет работать намного дольше (можно даже успеть выпить кружку чая)
     * Зато результат сравнения будет более ощутимым.
     */
    public static void binarySearchVsLinearSearchDemo() {
        int length = 50000;
        int bound = 5000;
        int[] array = generateArray(length, bound);

        //число, которое мы будем искать в массиве. Есть вероятность что оно не будет сгенерировано (массив генерируется случайно)
        //В таком случае - просто перезапустите код.
        int searchTarget = 4578;


        //замеряем производительность линейного поиска в неотсортированном массиве
        long beforeLinearSearch = System.currentTimeMillis();
        int result1 = linearSearch(array, searchTarget);
        long afterLinearSearch = System.currentTimeMillis();
        System.out.println(String.format("Время выполнения линейного поиска в неотсортированном массиве: %d мс", afterLinearSearch - beforeLinearSearch));
        System.out.println(result1 == -1 ? "Элемент не был найден (-1)." : "Элемент был найден по индексу " + result1);

        //замеряем производительность бинарного поиска в неотсортированном массиве
        //ВАЖНО - есть вероятность >99%, что бинарный поиск не сможет найти элемент впринципе и вернет -1
        //(т.к. не предназначен для использования в неотсортированных структурах данных)
        long beforeBinarySearch = System.currentTimeMillis();
        int result2 = binarySearch(array, searchTarget);
        long afterBinarySearch = System.currentTimeMillis();
        System.out.println(String.format("Время выполнения линейного поиска в отсортированном массиве: %d мс", afterBinarySearch - beforeBinarySearch));
        System.out.println(result2 == -1 ? "Элемент не был найден (-1)." : "Элемент был найден по индексу " + result2);


        //теперь отсортируем наш массив и снова попробуем повторить оба поиска
        //
        //здесь лучше воспользоваться втроенной в java сортировкой (алгоритм Quick sort - быстрая сортировка ) - Arrays.sort()
        //т.к. сортировка пузырьком для массива > 100000 элементов будет занимать уже очень серьезное время. Но на свой страх
        //и риск вы можете попробовать раскомментировать строку ниже запуститься с ней:
        //bubbleSort(array);
        Arrays.sort(array);


        //замеряем производительность линейного поиска в отсортированном массиве
        long beforeLinearSearchSorted = System.currentTimeMillis();
        int result3 = linearSearch(array, searchTarget);
        long afterLinearSearchSorted = System.currentTimeMillis();
        System.out.println(String.format("Время выполнения линейного поиска в отсортированном массиве: %d мс", afterLinearSearchSorted - beforeLinearSearchSorted));
        System.out.println(result3 == -1 ? "Элемент не был найден (-1)." : "Элемент был найден по индексу " + result3);

        //замеряем производительность бинарного поиска в отсортированном массиве
        long beforeBinarySearchSorted = System.currentTimeMillis();
        int result4 = binarySearch(array, searchTarget);
        long afterBinarySearchSorted = System.currentTimeMillis();
        System.out.println(String.format("Время выполнения линейного поиска в отсортированном массиве: %d мс", afterBinarySearchSorted - beforeBinarySearchSorted));
        System.out.println(result4 == -1 ? "Элемент не был найден (-1)." : "Элемент был найден по индексу " + result4 + '\n');


    }

    /**
     * Генерация массива случайных чисел.
     *
     * @param arrayLength - длина массива
     * @param randomBound - максимально возможное значение для генерируемого числа (верхняя граница)
     * @return - сгенерированный массив
     */
    public static int[] generateArray(int arrayLength, int randomBound) {
        Random random = new Random();
        int[] array = new int[arrayLength];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(randomBound + 1);
        }
        return array;
    }

    /**
     * Алгоритм сортировки пузырьком (bubble sort).
     *
     * @param inputArray - входной неупорядоченный массив, переданный для сортировки
     */
    public static void bubbleSort(int[] inputArray) {
        //внешний цикл - итерация по диапазонам поиска (с каждой итерацией диапазон становится уже и уже)
        for (int i = 0; i < inputArray.length - 1; i++) {
            //внутренний цикл - итерация по каждой паре соседних элементов внутри текущего диапазона
            for (int j = 0; j < inputArray.length - i - 1; j++) {
                //если левый элемент больше чем правый - значит их нужно поменять местами
                if (inputArray[j] > inputArray[j + 1]) {
                    //TODO раскомментируй эти строки если хочешь видеть каждую перемену мест элементов во время сортировки
                    //System.out.println(Arrays.toString(inputArray));
                    //System.out.println(String.format("Меняю местами элемент '%d' с элементом '%d'", inputArray[j], inputArray[j + 1]));
                    int temp = inputArray[j];
                    inputArray[j] = inputArray[j + 1];
                    inputArray[j + 1] = temp;
                }
            }
        }
    }

    /**
     * Алгоритм бинарного поиска.
     * Сложность O(log n)
     * Более производителен чем линейный поиск, но требует, чтобы передаваемый набор данных был отсортированным.
     *
     * @param inputArr - массив, в котором будет осуществляться поиск.
     * @param target - искомый элемент (то что мы ищем)
     * @return - индекс искомого элемента. Возвращает значение -1 если элемент не был найден.
     */
    public static int binarySearch(int[] inputArr, int target) {
        //левая граница диапазона поиска (это индекс внутри переданного массива)
        int left = 0;
        //правая граница диапазона поиска (это индекс внутри переданного массива)
        int right = inputArr.length - 1;
        //пока наш диапазон не "схлопнулся", т.е. пока внутри диапазона еще есть элементы - продолжаем искать
        while (left <= right) {
            //берем средний индекс внутри текущего диапазона
            int mid = (left + right) / 2;
            if (inputArr[mid] == target) { //если элемент найден (т.е. текущий элемент в середине диапазона равен искомому)
                return mid; //то возвращаем индекс найденного элемента
            } else if (inputArr[mid] < target) { //если текущий элемент меньше искомого - идем в правую половину текущего диапазона
                left = mid + 1;
            } else { //если текущий элемент больше искомого - двигаемся в левую половину текущего диапазона
                right = mid - 1;
            }
        }
        return -1; //элемент не был найден
    }

    /**
     * Алгоритм линейного поиска.
     * Сложность O(n)
     * Менее производителен чем бинарный поиск, но может работать с неупорядоченными наборами данных.
     *
     * @param inputArr - массив, в котором будет осуществляться поиск.
     * @param target - искомый элемент (то что мы ищем)
     * @return - индекс искомого элемента. Возвращает значение -1 если элемент не был найден.
     */
    public static int linearSearch(int[] inputArr, int target) {
        //перебираем последовательно, по порядку, с самого первого элемента
        for (int i = 0; i < inputArr.length; i++) {
            //если элемент найден (т.е. текущий элемент равен искомому)
            if (inputArr[i] == target) {
                //тогда возвращаем индекс найденного элемента
                return i;
            }
        }
        return -1; //элемент не был найден
    }
}
